module Fastbill
  module Automatic
    module Request
      class Info
        attr_accessor :service, :data, :api_key, :api_email

        def initialize(service, data)
          @service   = service

          @api_email = if data.key?(:api_email) then data[:api_email] else nil end
          @api_key   = if data.key?(:api_key)   then data[:api_key]   else nil end
          data.delete :api_email
          data.delete :api_key

          @data      = data
        end

        def url
          url = "/api/#{API_VERSION}/api.php"
          url
        end

        def path_with_params(path, params)
          unless params.empty?
            encoded_params = URI.encode_www_form(params)
            [path, encoded_params].join("?")
          else
            path
          end
        end
      end
    end
  end
end
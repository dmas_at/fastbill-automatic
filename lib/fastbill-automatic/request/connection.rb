module Fastbill
  module Automatic
    module Request
      class Connection
        attr_reader :https

        def initialize(request_info)
          @info = request_info
        end

        def setup_https
          @https             = Net::HTTP.new(API_BASE, Net::HTTP.https_default_port)
          @https.use_ssl     = true
          @https.verify_mode = OpenSSL::SSL::VERIFY_PEER
          @https.ca_file     = File.join(ROOT_PATH, "data/fastbill.crt")
        end

        def request
          https.start do |connection|
            https.request(https_request)
          end
        end

        protected

        def https_request
          https_request = Net::HTTP::Post.new(@info.url)
          https_request.basic_auth(if @info.api_email.nil? then Fastbill::Automatic.email else @info.api_email end, if @info.api_key.nil? then Fastbill::Automatic.api_key else @info.api_key end)
          body = {service: @info.service}
          body[(@info.service.include?('.get') ? :filter : :data)] = @info.data
          body[:limit] = 100
          https_request.body = body.to_json
          https_request
        end
      end
    end
  end
end

module Fastbill
  module Automatic
    module Services
      module Sendbypost
        module ClassMethods

          def sendbypost(id, api_data=nil)
            id_attribute = "#{self.name.split("::").last.downcase}_id".to_sym
            attributes = {}
            attributes[id_attribute] = id
            attributes = attributes.merge(api_data) unless api_data.nil?
            response = Fastbill::Automatic.request("#{self.name.split("::").last.downcase}.sendbypost", attributes)
            self.new(response["RESPONSE"])
          end
        end

        def self.included(base)
          base.extend(ClassMethods)
        end
      end
    end
  end
end
module Fastbill
  module Automatic
    module Services
      module DeleteItem
        module ClassMethods

          def delete(id, api_data = nil)
            attributes = {}
            attributes[:invoice_item_id] = id
            attributes = attributes.merge(api_data) unless api_data.nil?
            response = Fastbill::Automatic.request("item.delete", attributes)
            true
          end
        end

        def self.included(base)
          base.extend(ClassMethods)
        end
      end
    end
  end
end
module Fastbill
  module Automatic
    class Contact < Base
      include Fastbill::Automatic::Services::Update
      include Fastbill::Automatic::Services::Delete

      attr_accessor :contact_id, :customer_id, :country_code, :city, :term, :organization, :position, :salutation,
                    :first_name, :last_name, :address, :address_2, :zipcode, :phone, :phone_2, :fax, :mobile, :email,
                    :currency_code, :vat_id

    end
  end
end
